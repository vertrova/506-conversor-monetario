#!/bin/bash
echo Conversor monetario
echo Moeda base:
read base
echo Moeda a converter
read destino
echo Quantidade
read qtde

URL="https://api.exchangeratesapi.io/latest?base=$base&symbols=$destino"

echo $URL

echo "Calculando conversao..."

valor=`curl $URL | jq ".rates.$destino"`

echo "Valor da cotacao $valor"

total=$(python -c "print $valor * $qtde")

echo "Valor total convertido: $total"

